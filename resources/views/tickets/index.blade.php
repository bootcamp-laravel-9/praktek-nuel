@extends('layout/main')

@section('content')
    <div class="container mt-5">
        <h2>Ticket Report</h2>
        <div class="mb-3">
            <label for="categoryFilter" class="form-label">Category:</label>
            <select id="categoryFilter" class="form-select">
                <option value="">All Categories</option>
                @foreach ($categories as $category)
                    <option value="{{ $category }}">{{ $category }}</option>
                @endforeach
            </select>
        </div>
        <div class="row mb-3">
            <div class="col">
                <label for="dateBeforeFilter" class="form-label">Date Before:</label>
                <input type="date" id="dateBeforeFilter" class="form-control">
            </div>
            <div class="col">
                <label for="dateAfterFilter" class="form-label">Date After:</label>
                <input type="date" id="dateAfterFilter" class="form-control">
            </div>
        </div>
        <div class="mb-3">
            <div class="col">
                <input type="text" id="searchInput" placeholder="Search here" class="form-control">
            </div>
        </div>
        <table id="ticketTable" class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Ticket No</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Date</th>
                    <th>Category</th>
                    <th>Total Ticket</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tickets as $ticket)
                    <tr>
                        <td>{{ $ticket->id }}</td>
                        <td>{{ $ticket->no_ticket }}</td>
                        <td>{{ $ticket->nama }}</td>
                        <td>{{ $ticket->email }}</td>
                        <td>{{ $ticket->no_telp }}</td>
                        <td>{{ $ticket->address }}</td>
                        <td>{{ $ticket->date_ticket }}</td>
                        <td>{{ $ticket->details[0]->category->name ?? '' }}</td>
                        <td>{{ $ticket->details[0]->total_ticket ?? '' }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-end">
            <a href="{{ url('/createTicket') }}" class="btn btn-primary">LAPORAN HARI INI</a>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            var table = $('#ticketTable').DataTable({
                "paging": true, // Enable pagination
                "language": {
                    "lengthMenu": "",
                    "search": "",
                },

                "dom": "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 d-flex justify-content-end'p>>",
            });

            // Add custom filtering function
            $('#categoryFilter, #dateBeforeFilter, #dateAfterFilter, #searchInput').on('change keyup', function() {
                table.draw();
            });

            $.fn.dataTable.ext.search.push(
                function(settings, data, dataIndex) {
                    var categoryFilter = $('#categoryFilter').val();
                    var dateBeforeFilter = $('#dateBeforeFilter').val();
                    var dateAfterFilter = $('#dateAfterFilter').val();
                    var searchInput = $('#searchInput').val();
                    var category = data[7]; // Assuming Category is at index 7
                    var date = data[6]; // Assuming Date is at index 6

                    // Apply custom filtering logic
                    if ((categoryFilter === '' || categoryFilter === category) &&
                        (dateBeforeFilter === '' || dateBeforeFilter <= date) &&
                        (dateAfterFilter === '' || dateAfterFilter >= date) &&
                        (searchInput === '' || data.some(column => column.includes(searchInput)))) {
                        return true;
                    }
                    return false;
                }
            );

            // Reset search input and column filters on table redraw
            $('#ticketTable').on('draw.dt', function() {
                table.search('').columns().search('').draw();
            });
        });
    </script>
@endsection
