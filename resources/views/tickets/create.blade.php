@extends('layout.main')

{{-- Set the title and other relevant sections --}}
@section('title', 'Create Ticket')

@section('content')
    <div class="container mt-5">
        <h2>Create Ticket</h2>
        <form action="{{ url('/createTicket') }}" method="POST">
            @csrf {{-- CSRF token for security --}}
            <div class="mb-3">
                <label for="nama" class="form-label">Name</label>
                <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" value="{{ old('nama') }}" required>
                @error('nama')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">Email</label>
                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}">
                @error('email')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="no_telp" class="form-label">Phone</label>
                <input type="text" class="form-control @error('no_telp') is-invalid @enderror" id="no_telp" name="no_telp" value="{{ old('no_telp') }}">
                @error('no_telp')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="address" class="form-label">Address</label>
                <input type="text" class="form-control" id="address" name="address" value="{{ old('address') }}" required>
                @error('address')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="date_ticket" class="form-label">Date</label>
                <input type="date" class="form-control" id="date_ticket" name="date_ticket" value="{{ old('date_ticket') }}" required>
                @error('date_ticket')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="ticket_category_id" class="form-label">Category</label>
                <select id="ticket_category_id" name="details[0][ticket_category_id]" class="form-control" value="{{ old('ticket_category_id') }}" required>
                    <option value="">Select Category</option>
                    @foreach ($categories as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3">
                <label for="total_ticket" class="form-label">Total Ticket</label>
                <input type="number" min="1" class="form-control" id="total_ticket" name="details[0][total_ticket]" value="{{ old('total_ticket') }}" required>
                @error('total_ticket')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="mb-3">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
        <script>
            // Get the current date in YYYY-MM-DD format
            var today = new Date().toISOString().split('T')[0];
            // Set the minimum date for the input field
            document.getElementById('date_ticket').min = today;
        </script>
    </div>
@endsection

