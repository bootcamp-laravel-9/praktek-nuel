@extends('layout/main')
@section('content')
@section('title', 'Users')
@section('menu-users', 'active')

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Users</title>
    <!-- DataTables CSS -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <!-- DataTables JS -->
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <style>
        /* Add your existing CSS styles here */

        /* Header styles */
        .header {
            background-color: purple;
            color: white;
            text-align: center;
            padding: 20px;
        }

        /* Table styles */
        .table {
            width: 100%;
            border-collapse: collapse;
        }

        .table th,
        .table td {
            padding: 10px;
            text-align: left;
        }

        .table th {
            background-color: blue;
            color: white;
        }

        .table tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        /* Action button styles */
        .btn {
            padding: 5px 10px;
            border: none;
            cursor: pointer;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4>Master Admin</h4>
                    </div>
                    <div class="card-body">
                        <div class="mb-3">
                            <a href="{{ url('/createUser') }}" class="btn btn-primary">+ TAMBAH</a>
                        </div>
                        <table id="usersTable" class="table">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>NAMA</th>
                                    <th>EMAIL</th>
                                    <th>ACTIONS</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $user->id }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>
                                            <a href="{{url('/updateUser/' . $user->id)}}" class="btn btn-warning">Edit</a>
                                            <a href="{{url('/deleteUser/' . $user->id)}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this user?')">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script>
    $(document).ready(function() {
        $('#usersTable').DataTable({
            "paging": true, // Enable pagination
            "lengthChange": false, // Disable page size change
            "searching": false, // Disable search feature
            "ordering": true, // Enable ordering (you can customize sorting columns)
            "info": true, // Show table information
            "autoWidth": false // Disable automatic column width calculation
        });
    });
</script>
</html>

@endsection
