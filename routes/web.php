<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect('login');
});

Route::group(['middleware' => ['guest']], function () {
    Route::get('login', 'AuthController@login');
    Route::post('login', 'AuthController@login_process');
});

Route::group(['middleware' => ['isSessionValid']], function () {
    Route::get('dashboard', 'DashboardController@dashboard');
    Route::get('users', 'UserController@getUsers');
    Route::get('createUser', 'UserController@createUser');
    Route::get('updateUser/{id}', 'UserController@updateUser');
    Route::post('createUserProcess', 'UserController@createUserProcess');
    Route::put('updateUserProcess/{id}', 'UserController@updateUserProcess');
    Route::get('deleteUser/{id}', 'UserController@deleteUser');
    Route::get('tickets', 'ConsumeApiController@index');
    Route::get('createTicket', 'ConsumeApiController@createTicket');
    Route::post('createTicket', 'ConsumeApiController@create');
    // Logout Route
    Route::get('logout', 'AuthController@logout');
});
