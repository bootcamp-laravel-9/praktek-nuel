<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\TicketDetail;

class TicketDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $details = [
            [
                'ticket_header_id' => 1,
                'ticket_category_id' => 1,
                'total_ticket' => 10,
            ],
            [
                'ticket_header_id' => 2,
                'ticket_category_id' => 2,
                'total_ticket' => 10,
            ],
        ];

        DB::beginTransaction();

        foreach ($details as $detail) {
            TicketDetail::firstOrCreate($detail);
        }

        DB::commit();
    }
}
