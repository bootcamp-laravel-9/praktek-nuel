<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\TicketHeader;

class TicketHeaderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $headers = [
            [
                'no_ticket' => 'TCK-0001',
                'nama' => 'John',
                'email' => 'john@example.com',
                'no_telp' => '1234567890',
                'address' => '123 Main St, Anytown, USA',
                'date_ticket' => '2024-03-02'
            ],
            [
                'no_ticket' => 'TCK-0002',
                'nama' => 'Jono',
                'email' => 'Jono@example.com',
                'no_telp' => '0987654321',
                'address' => '456 Elm St, Anothertown, USA',
                'date_ticket' => '2024-03-02'
            ],
        ];

        DB::beginTransaction();

        foreach ($headers as $header) {
            TicketHeader::firstOrCreate($header);
        }

        DB::commit();
    }
}
