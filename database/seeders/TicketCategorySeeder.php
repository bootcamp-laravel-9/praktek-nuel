<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\TicketCategory;

class TicketCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'Ekonomi',
                'detail' => 'Ini adalah ticket ekonomi',
            ],
            [
                'name' => 'Bisnis',
                'detail' => 'Ini adalah ticket bisnis',
            ]
        ];

        DB::beginTransaction();

        foreach ($categories as $category) {
            TicketCategory::firstOrCreate($category);
        }

        DB::commit();
    }
}
