<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_headers', function (Blueprint $table) {
            $table->id();
            $table->string('no_ticket');
            $table->string('nama');
            $table->string('email');
            $table->integer('no_telp');
            $table->string('address');
            $table->date('date_ticket');
            $table->date('created_at');
            $table->date('updated_at');
            $table->date('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_headers');
    }
};
