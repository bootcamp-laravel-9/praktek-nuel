<?php

namespace App\Repositories;

use App\Http\Resources\BrandResource;
use App\Models;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthRepository
{
    protected $user;
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    /**
     * @param array<string, mixed> $data
     * @return User
     */
    public function login($request)
    {
        $credentials = $request->only('email', 'password');
        if (!Auth::attempt($credentials)) {
            return response()->json(['error' => 'Login failed, please check yout credentials'], 400);
        }
        $user = User::where('email', $request->email)->first();
        $user['token'] = $user->createToken(config('app.name'))->plainTextToken;
        return response()->json([
            'status' => true,
            'message' => 'Login successful',
            'data' => $user
        ], 200);
    }
}
