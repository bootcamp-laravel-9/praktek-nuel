<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Models\TicketCategory;

class ConsumeApiController extends Controller
{
    public function index(){
        $categories = TicketCategory::pluck('name');

        // Create an API request to fetch tickets
        $apiRequest = Request::create(url('api/ticket'), 'GET');

        // Send the API request
        $apiResponse = app()->handle($apiRequest);

        // Check if the API request was successful
        if ($apiResponse->getStatusCode() === 200) {
            $tickets = json_decode($apiResponse->getContent());

            // You can now use the $tickets data as needed, such as passing it to a view
            return view('tickets.index', ['tickets' => $tickets , 'categories' => $categories]);
        } else {
            // Handle API request failure
            return response()->json(['error' => 'Failed to fetch tickets'], $apiResponse->getStatusCode());
        }
    }

    public function createTicket(){
        $categories = TicketCategory::pluck('name', 'id');
        return view('tickets.create', compact('categories'));
    }

    public function create(Request $request){
        $validatedData = $request->validate([
            'nama' => 'required|string|regex:/^[^0-9]*$/',
            'email' => 'required|email',
            'no_telp' => 'required|string',
            'address' => 'required|string',
            'date_ticket' => 'required|date',
        ]);

        $requestData = [
            'nama' => $validatedData['nama'],
            'email' => $validatedData['email'],
            'no_telp' => $validatedData['no_telp'],
            'address' => $validatedData['address'],
            'date_ticket' => $validatedData['date_ticket'],
            'details' => $request->input('details'),
        ];

        $apiRequest = Request::create(url('api/create-ticket'), 'POST', $requestData);

        $apiResponse = app()->handle($apiRequest);
        if ($apiResponse->getStatusCode() === 200) {
            $responseArray = json_decode($apiResponse->getContent(), true);
            return redirect()->intended('createTicket')->with('message', 'Ticket created/updated successfully');
        } else {
            $errorResponse = json_decode($apiResponse->getContent(), true);
            return back()->withInput()->with('error', $errorResponse['error'] ?? 'Failed to create/update ticket');
        }
    }
}
