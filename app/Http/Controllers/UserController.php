<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function getUsers(){
        $users = User::all();
        return view('user.index', [
            'users' => $users,
        ]);
    }

    public function createUser(){
        return view('user.create');
    }

    public function createUserProcess(Request $request) {
        $this->validate($request, [
            'name' => ['required', 'regex:/^[^0-9]*$/'],
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => ['required', 'confirmed', 'min:8', 'max:15',],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request['password']),
        ]);

        return redirect()->intended('users')->with('message', 'Berhasil membuat data user.')->withInput($request->except('password'));
    }

    public function updateUser($id){
        $user = User::find($id);
        return view('user.update', compact('user'));
    }

    public function updateUserProcess(Request $request, User $user){
        $this->validate($request, [
            'name' => ['required', 'regex:/^[^0-9]*$/'],
            'email' => ['required', 'email'],
            'password' => [
                'nullable', 'confirmed', 'min:8', 'max:15',
            ],
        ]);

        $user = $user->find($request->id);

        $user->name = $request->name;
        $user->email = $request->email;

        if ($request->password) {
            $user->password = Hash::make($request->password);
        }

        $user->save();

        return redirect()->intended('users')->with('message', 'Data Profile Telah Berhasil Di Update');
    }

    public function deleteUser($id)
    {
        $detail = User::destroy($id);
        return redirect()->intended('users')->with('message', 'Data Profile Telah Berhasil Di Hapus');
    }
}
