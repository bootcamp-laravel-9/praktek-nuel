<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Repositories\AuthRepository;
use Illuminate\Http\Request;
use App\Http\Requests\AuthRequest;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function __construct(AuthRepository $AuthRepository){
        $this->AuthRepository = $AuthRepository;
    }

    public function apilogin(AuthRequest $request) {
        try {
            $user = $this->AuthRepository->login($request);
            return $user;
        } catch (\Exception $e)
        {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    public function login() {
        return view('auth.login');
    }

    public function login_process(AuthRequest $request) {
        $apiRequest = Request::create(url('api/login'), 'POST', [
            'email' => $request->email,
            'password' => $request->password,
        ]);
        $apiRequest->headers->set('Accept', 'application/json');

        $response = app()->handle($apiRequest);

        $credentials = $request->only('email', 'password');

        if ($response->getStatusCode() == 200) {
            $user = User::where('email', $credentials['email'])->first();
            $token = $user->createToken(config('app.name'))->plainTextToken;
            $request->session()->put('LoginSession', $token);

            return redirect()->intended('dashboard')->with('message', 'Login success');
        } else {
            return redirect()->back()->with('error', 'Login failed, please check your credentials');
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect()->intended('login');
    }
}
